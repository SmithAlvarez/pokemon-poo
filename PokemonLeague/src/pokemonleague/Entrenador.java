package pokemonleague;
/**
 * Clase Entrenador para crear el jugador y los retadores del juego.
 */
public class Entrenador {
    
    /**
     * Declaro los atributos de la clase entrenador.
     */    
    private String nombre, origen;    
    private int edad, experiencia = 0;     
    
    
    /**
     * Declaro Constructor por defecto.
     */
    public Entrenador(){
        
    }
    
    /**
     * Constructor con parametros.
     * @param nombre
     * @param origen
     * @param Edad
     */
    public Entrenador(String nombre, String origen, int Edad) {
        this.nombre = nombre;
        this.origen = origen;
        this.edad = Edad;        
    }
    
    /**
     * Arreglo de pokemones para el entrenador creado.
     */
    Pokemons [] pokemones_propios = new Pokemons[3];
    
    /**
     * Getters and Setters.
     */

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the origen
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    public void setOrigen(String origen) {
        this.origen = origen;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the experiencia
     */
    public int getExperiencia() {
        return experiencia;
    }

    /**
     * @param experiencia the experiencia to set
     */
    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    /**
     * Sobreescritura de los métodos.
     */
    
    @Override
    public String toString(){
    return "Nombre: "+getNombre()+"/Origen: "+getOrigen()+"/ Edad: "+getEdad()+"/ Experiencia: "+getExperiencia();
    
    }
    
}
