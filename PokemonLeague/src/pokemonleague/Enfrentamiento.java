package pokemonleague;
import java.util.Scanner;

/**
 * Clase Enfrentamiento para crear los combates del juego.
 */
public class Enfrentamiento {
    
        /**
        * Atributos de la clase Enfrentamiento.
        */
	Pokemons pokemonTu,pokemonRival;

        
        /**
        * Método para hallar el valor del turno correspondiente al jugador que 
        * empezara primero en la batalla.
        * @param nombreP1 jugador 1
        * @param nombreP2 jugador 2
        */	
	public int InicioLucha(String nombreP1,String nombreP2) {                
                /**
                * Uso el método random, para asignar un valor entero aleatorio
                * para cada jugador.
                */
		int turno = (int)(Math.random()*2)+1;
		if(turno == 1) {
			System.out.println(nombreP1 + " Vas primero");
			return 1;
		}else if (turno == 2) {
			System.out.println(nombreP2 + " Vas primero");
			return 2;
		}else {
			System.out.println("error selec turno");
			return 0;
		}	
	}
	
	/**
        * Método para la lucha de dos pokemones.
        * @param turnoPlayer
        */
	public int Lucha(int turnoPlayer){		
                /**
                * Valor del turno hallado con el método InicioLucha().
                */
                int turno =  turnoPlayer;
                
                /**
                * Turno jugador 1.
                */		
		if(turno==1) {
			System.out.println(this.pokemonTu.getNombre() + " Va a atacar escoje una de sus habilidades");
			for(int i=0;i<this.pokemonTu.getHabilidades().length;i++) {
				System.out.println((i+1)+" "+this.pokemonTu.getHabilidades()[i].nombre);
			}
                        
                        /**
                         * Menu para escoger una de las habilidades del Pokemon 
                         * seleccionado.
                         */			
			Scanner tcl = new Scanner(System.in);
			int habilidad =  tcl.nextInt();
			System.out.println(this.pokemonTu.getNombre() + " Va a usar "+this.pokemonTu.getHabilidades()[(habilidad-1)].nombre);
                        
                        /**
                         * Casos en los cuales un tipo sea superior o inferior 
                         * al otro, esto afectara el dano de las habilidades.
                         */			
			switch(this.pokemonTu.getTipo()) {
			   	case "Trueno":
			   		if(this.pokemonRival.getTipo().equals("Agua")){
						this.pokemonRival.setVida(this.pokemonRival.getVida()-(this.pokemonTu.getHabilidades()[(habilidad-1)].poder+1));
						System.out.println("Ha sido super efectivo");
						break;
			   		}else if(this.pokemonRival.getTipo().equals("Fuego")){
						this.pokemonRival.setVida(this.pokemonRival.getVida() - (this.pokemonTu.getHabilidades()[(habilidad-1)].poder-1));
						System.out.println("No ha sido efectivo");
						break;
			   		}else {
						this.pokemonRival.setVida(this.pokemonRival.getVida()-this.pokemonTu.getHabilidades()[(habilidad-1)].poder);
						System.out.println("Le has dado");
						break;
			   		}
			   	case "Agua":
			   		if(this.pokemonRival.getTipo().equals("Veneno")){
						this.pokemonRival.setVida(this.pokemonRival.getVida() - (this.pokemonTu.getHabilidades()[(habilidad-1)].poder+1));
						System.out.println("Ha sido super efectivo");
						break;
			   		}else if(this.pokemonRival.getTipo().equals("Trueno")){
						this.pokemonRival.setVida(this.pokemonRival.getVida() - (this.pokemonTu.getHabilidades()[(habilidad-1)].poder-1));
						System.out.println("No ha sido efectivo");
						break;
			   		}else {
						this.pokemonRival.setVida(this.pokemonRival.getVida() - this.pokemonTu.getHabilidades()[(habilidad-1)].poder);
						System.out.println("Le has dado");
						break;
			   		}
			   	case "Viento":
			   		if(this.pokemonRival.getTipo().equals("Fuego")){
						this.pokemonRival.setVida(this.pokemonRival.getVida() - (this.pokemonTu.getHabilidades()[(habilidad-1)].poder+1));
						System.out.println("Ha sido super efectivo");
						break;
			   		}else if(this.pokemonRival.getTipo().equals("Tierra")){
						this.pokemonRival.setVida(this.pokemonRival.getVida() - (this.pokemonTu.getHabilidades()[(habilidad-1)].poder-1));
						System.out.println("No ha sido efectivo");
						break;
			   		}else {
						this.pokemonRival.setVida((this.pokemonRival.getVida() - this.pokemonTu.getHabilidades()[(habilidad-1)].poder));
						System.out.println("Le has dado");
						break;
			   		}
			   	case "Fuego":
			   		if(this.pokemonRival.getTipo().equals("Trueno")){
						this.pokemonRival.setVida(this.pokemonRival.getVida() - (this.pokemonTu.getHabilidades()[(habilidad-1)].poder+1));
						System.out.println("Ha sido super efectivo");
						break;
			   		}else if(this.pokemonRival.getTipo().equals("Viento")){
			   			this.pokemonRival.setVida(this.pokemonRival.getVida()-(this.pokemonTu.getHabilidades()[(habilidad-1)].poder-1));
						System.out.println("No ha sido efectivo");
						break;
			   		}else {
						this.pokemonRival.setVida(this.pokemonRival.getVida()-this.pokemonTu.getHabilidades()[(habilidad-1)].poder);
						System.out.println("Le has dado");
						break;
			   		}
			   	case "Veneno":
			   		if(this.pokemonRival.getTipo().equals("Tierra")){
						this.pokemonRival.setVida(this.pokemonRival.getVida() - (this.pokemonTu.getHabilidades()[(habilidad-1)].poder+1));
						System.out.println("Ha sido super efectivo");
						break;
			   		}else if(this.pokemonRival.getTipo().equals("Agua")){
			   			this.pokemonRival.setVida(this.pokemonRival.getVida()-(this.pokemonTu.getHabilidades()[(habilidad-1)].poder-1));
						System.out.println("No ha sido efectivo");
						break;
			   		}else {
						this.pokemonRival.setVida(this.pokemonRival.getVida()-this.pokemonTu.getHabilidades()[(habilidad-1)].poder);
						System.out.println("Le has dado");
						break;
			   		}
			   	case "Tierra":
			   		if(this.pokemonRival.getTipo().equals("Viento")){
						this.pokemonRival.setVida(this.pokemonRival.getVida() - (this.pokemonTu.getHabilidades()[(habilidad-1)].poder+1));
						System.out.println("Ha sido super efectivo");
						break;
			   		}else if(this.pokemonRival.getTipo().equals("Veneno")){
			   			this.pokemonRival.setVida(this.pokemonRival.getVida()-(this.pokemonTu.getHabilidades()[(habilidad-1)].poder-1));
						System.out.println("No ha sido efectivo");
						break;
			   		}else {
						this.pokemonRival.setVida(this.pokemonRival.getVida()-this.pokemonTu.getHabilidades()[(habilidad-1)].poder);
 						System.out.println("Le has dado");
						break;
			   		}
			}
			
                        /**
                         * Codigo para evaluar el rival durante el combate.
                         * Si se ha cumplido, el rival ha perdido el combate.
                         */
                        System.out.println(this.pokemonRival.getNombre() + " ahora tiene "+this.pokemonRival.getVida()+" puntos de vida");
			if(this.pokemonRival.getVida()<=0){
                                
                                /**
                                 * Mensaje de perdida de combate para el rival.
                                 */
				System.out.println(this.pokemonRival.getNombre() + " a perdido el combate");
                                
                                /**
                                 * Switch case para incrementar el nivel del 
                                 * pokemon correspondiente al jugador, despues
                                 * del combate.
                                 */
				switch(this.pokemonTu.getNivel()) {
					case 1:
						if(this.pokemonTu.getVida()<=15) {
						int vidaRecuperar = (int)((15-this.pokemonTu.getVida())/2);
						if(this.pokemonTu.getVida() + vidaRecuperar > 15) {
							this.pokemonTu.setVida(15);
						}
						else {
							this.pokemonTu.setVida(this.pokemonTu.getVida() + vidaRecuperar);
						}
						break;
						}
					case 2:
						if(this.pokemonTu.getVida()<=20) {
						int vidaRecuperar = (int)((20-this.pokemonTu.getVida())/2);
						if(this.pokemonTu.getVida() + vidaRecuperar > 20) {
							this.pokemonTu.setVida(20);
						}
						else {
							this.pokemonTu.setVida(this.pokemonTu.getVida() + vidaRecuperar);	
						}						
							break;
						}
					case 3:
						if(this.pokemonTu.getVida()<=25){
						int vidaRecuperar = (int)((25-this.pokemonTu.getVida())/2);
						if(this.pokemonTu.getVida() + vidaRecuperar > 25) {
							this.pokemonTu.setVida(25);
						}
						else {
							this.pokemonTu.setVida(this.pokemonTu.getVida() + vidaRecuperar);
						}						
						break;
						}
				}
				this.pokemonTu.setExperiencia((this.pokemonTu.getExperiencia()+50));
				this.pokemonTu.levelup(this.pokemonTu.getNivel(), this.pokemonTu.getTipo());
				System.out.println(this.pokemonTu.getNombre()+" a ganado el combate, ahora tiene "+this.pokemonTu.getVida()+" ya que ha recuperado el 50% de su vida perdida");
				return 1;
			}else {
				/**
                                 * Instruccion para cambio al siguiente jugador.
                                 */
				return this.Lucha(2);
			}
		/**
                 * Turno jugador 2.
                 */                
		}else if(turno==2) {
			System.out.println(this.pokemonRival.getNombre() + " Va a atacar escoje una de sus habilidades");
			for(int i=0;i<this.pokemonRival.getHabilidades().length;i++) {
				System.out.println((i+1)+" "+this.pokemonRival.getHabilidades()[i].nombre);
			}
			
                        /**
                         * Menu para escoger una de las habilidades del Pokemon 
                         * seleccionado.
                         */
			Scanner tcl = new Scanner(System.in);
			int habilidad =  tcl.nextInt();
			System.out.println(this.pokemonRival.getNombre() + " Va a usar "+this.pokemonRival.getHabilidades()[(habilidad-1)].nombre);
			
                        /**
                         * Switch case, para evaluar los casos en los cuales un 
                         * tipo sea superior o inferior al otro, esto afectara 
                         * el dano de las habilidades.
                         */                        
			switch(this.pokemonRival.getTipo()) {
		   	case "Trueno":
		   		if(this.pokemonTu.getTipo().equals("Agua")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder+1));
					System.out.println("Ha sido super efectivo");
					break;
		   		}else if(this.pokemonTu.getTipo().equals("Fuego")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder-1));
					System.out.println("No ha sido efectivo");
					break;
		   		}else {
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder));
					System.out.println("Le has dado");
					break;
		   		}
		   	case "Agua":
		   		if(this.pokemonTu.getTipo().equals("Veneno")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder+1));
					System.out.println("Ha sido super efectivo");
					break;
		   		}else if(this.pokemonTu.getTipo().equals("Trueno")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder-1));
					System.out.println("No ha sido efectivo");
					break;
		   		}else {
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder));
					System.out.println("Le has dado");
					break;
		   		}
		   	case "Viento":
		   		if(this.pokemonTu.getTipo().equals("Fuego")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder+1));
					System.out.println("Ha sido super efectivo");
					break;
		   		}else if(this.pokemonRival.getTipo().equals("Tierra")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder-1));
					System.out.println("No ha sido efectivo");
					break;
		   		}else {
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder));
					System.out.println("Le has dado");
					break;
		   		}
		   	case "Fuego":
		   		if(this.pokemonTu.getTipo().equals("Trueno")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder+1));
					System.out.println("Ha sido super efectivo");
					break;
		   		}else if(this.pokemonTu.getTipo().equals("Viento")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder-1));
					System.out.println("No ha sido efectivo");
					break;
		   		}else {
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder));
					System.out.println("Le has dado");
					break;
		   		}
		   	case "Veneno":
		   		if(this.pokemonTu.getTipo().equals("Tierra")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder+1));
					System.out.println("Ha sido super efectivo");
					break;
		   		}else if(this.pokemonTu.getTipo().equals("Agua")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder-1));
					System.out.println("No ha sido efectivo");
					break;
		   		}else {
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder));
					System.out.println("Le has dado");
					break;
		   		}
		   	case "Tierra":
		   		if(this.pokemonTu.getTipo().equals("Viento")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder+1));
					System.out.println("Ha sido super efectivo");
					break;
		   		}else if(this.pokemonTu.getTipo().equals("Veneno")){
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder-1));
					System.out.println("No ha sido efectivo");
					break;
		   		}else {
					this.pokemonTu.setVida(this.pokemonTu.getVida() - (this.pokemonRival.getHabilidades()[(habilidad-1)].poder));
					System.out.println("Le has dado");
					break;
		   		}
			}
                                                
                        /**
                         * Codigo para evaluar el jugador durante el combate.
                         * Si se ha cumplido, el jugador ha perdido el combate.
                         */
			System.out.println(this.pokemonTu.getNombre() + " ahora tiene "+this.pokemonTu.getVida()+" puntos de vida");
			if(this.pokemonTu.getVida()<=0) {
                            
                                /**
                                 * Mensaje de perdida de combate para el jugador.
                                 */
				System.out.println(this.pokemonTu.getNombre() + " ha perdido el combate");
				
                                /**
                                 * Switch case para incrementar el nivel del 
                                 * pokemon correspondiente al jugador, despues
                                 * del combate.
                                 */
				switch(this.pokemonRival.getNivel()) {
				case 1:
					if(this.pokemonRival.getVida()<=15) {
					int vidaRecuperar = (int)((15-this.pokemonRival.getVida())/2);
					if(this.pokemonRival.getVida() + vidaRecuperar > 15) {
						this.pokemonRival.setVida(15);
					}else {
						this.pokemonRival.setVida(this.pokemonRival.getVida() + vidaRecuperar);
					}
					break;
					}
				case 2:
					if(this.pokemonRival.getVida()<=20) {
					int vidaRecuperar = (int)((20-this.pokemonRival.getVida())/2);
					this.pokemonRival.setVida(this.pokemonRival.getVida() + vidaRecuperar);
					if(this.pokemonRival.getVida() + vidaRecuperar > 20) {
						this.pokemonRival.setVida(20);
					}else {
						this.pokemonRival.setVida(this.pokemonRival.getVida() + vidaRecuperar);
					}
					break;
					}
				case 3:
					if(this.pokemonRival.getVida()<=25){
					int vidaRecuperar = (int)((25-this.pokemonRival.getVida())/2);
					if(this.pokemonRival.getVida() + vidaRecuperar > 25) {
						this.pokemonRival.setVida(25);
					}else {
						this.pokemonRival.setVida(this.pokemonRival.getVida() + vidaRecuperar);
					}
					break;
					}
			}
			System.out.println(this.pokemonRival.getNombre()+" a ganado el combate, ahora tiene "+this.pokemonRival.getVida()+" ya que ha recuperado el 50% de su vida perdida");
			return 2;
			}else{
				return this.Lucha(1);
			}
		}else {
			System.out.println("error turno");
			return 3;
		}
	}
	
	public Enfrentamiento(Pokemons pokemonTu,Pokemons pokemonRival) {
		this.pokemonTu = pokemonTu;
		this.pokemonRival = pokemonRival;
	}
	
}
