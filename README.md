***********************

The Pokemon League

Realizar un campeonato Pokemon.

El Campeonato consiste en 
• Se registran los maestros pokemon (Se trabaja con 6 maestross) Opción de n.
• Cada maestro tendria máximo 3 Pokemon.
• Las batallas enfrentan 1 contra 1. / El ganador Avanza.
• En la siguiente batalla, el pokemon recupera Max el 50% de la vida que perdió en la anterior batalla.

***********************
Requerimientos
***********************

• Usar POO.
• Usar GitLab.
• Al iniciar el campeonato:
 • Solicitar datos (Pokemones y Entrenadores).
• Durante cada batalla: 
 • Mostrar menú de opciones del entrenador.
• Luego de la batalla:
 • Mostrar ganador y su estado.

***********************
Extras
***********************

Puntos adicionales por generar la documentación
(diagrama clases / casos uso / usar herramientas GitLab).

***********************
Clase entrenador, pokemon.